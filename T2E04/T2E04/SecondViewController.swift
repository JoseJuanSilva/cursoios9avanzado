//
//  SecondViewController.swift
//  T2E04
//
//  Created by José Juan Silva Gamiño on 04/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIViewControllerTransitioningDelegate {
    var fade : FadeTransition!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fade = FadeTransition()
    }

   
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFadeTransition"{
            let destVC = segue.destinationViewController
            destVC.transitioningDelegate = self 
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
              // Dispose of any resources that can be recreated.
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.fade
    }

}
