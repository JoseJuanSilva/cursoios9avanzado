//
//  ViewController.swift
//  T2E03
//
//  Created by José Juan Silva Gamiño on 01/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func openSecondViewController(sender: UIButton) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let destViewController = main.instantiateViewControllerWithIdentifier("NuevoViewController")
        self.presentViewController(destViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func openSecondViewControllerFromXib(sender: UIButton) {
        let destViewControler = UIViewController(nibName: "NewViewController", bundle: NSBundle.mainBundle())
        self.presentViewController(destViewControler, animated: true, completion: nil)
    }


}

