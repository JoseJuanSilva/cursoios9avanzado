//
//  Videojuego.swift
//  T3E03
//
//  Created by José Juan Silva Gamiño on 06/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit
import SpriteKit

class Videojuego: SKScene, SKPhysicsContactDelegate {
    let score = SKLabelNode(fontNamed: "Chalkduster")
    var target : SKSpriteNode!
    var puntos = 0
    var value: CGFloat = 0.0
    enum ColliderType: UInt32{
        case Player = 0
        case Target = 1
    }
    
    
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        let size2 = CGRectMake(0.0, 0.0, self.frame.width, self.frame.height)
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: size2)
        
        let player = SKSpriteNode(color: UIColor.redColor(), size: CGSizeMake(100, 50))
        player.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(100, 50))
        player.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        player.name = "Player"
        player.physicsBody?.categoryBitMask = ColliderType.Player.rawValue
        player.physicsBody?.collisionBitMask = ColliderType.Target.rawValue
        player.physicsBody?.contactTestBitMask = ColliderType.Target.rawValue
        self.addChild(player)
        
        target = SKSpriteNode(color: UIColor.greenColor(), size: CGSizeMake(40, 20))
        target.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(40, 20))
        target.physicsBody?.dynamic = false
        target.position = CGPointMake(200, 530)
        target.name = "Target"
        target.physicsBody?.categoryBitMask = ColliderType.Target.rawValue
        target.physicsBody?.collisionBitMask = ColliderType.Player.rawValue
        target.physicsBody?.contactTestBitMask = ColliderType.Player.rawValue
        self.addChild(target)
        
        let movimiento = SKAction.sequence([SKAction.moveTo(CGPointMake(0, 530), duration: 2.0),SKAction.moveTo(CGPointMake(self.frame.size.width, 530), duration: 2.0)])
        let movimientoConstante = SKAction.repeatActionForever(movimiento)
        target.runAction(movimientoConstante)
        
        let slider = UISlider(frame: CGRectMake(0,0,325,100))
        
       
        
        slider.addTarget(self, action: #selector(self.manejarSlider) , forControlEvents: UIControlEvents.ValueChanged)
        
        slider.minimumValue = -50
        slider.maximumValue = 50
        view.addSubview(slider)
        
        score.text = "Puntuacion: \(puntos)"
        score.name = "Puntos"
        score.fontSize = 50
        score.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        self.addChild(score)
    }
    
    
    func manejarSlider(slider: UISlider){
      //  self.childNodeWithName("Player")?.physicsBody?.applyForce(CGVectorMake(CGFloat(slider.value * 100), 0))
        let proporcion  = self.frame.width / 100
        var xPoint = (self.frame.width / 2) + proporcion * CGFloat(slider.value)
        if xPoint < 10 {xPoint = 10} ;
        if xPoint > self.frame.width  - 10{
            xPoint = self.frame.width - 10 }
        
        let posActual = self.childNodeWithName("Player")?.position.x
        print ("x: " + xPoint.description +  "   actual:" +  (posActual?.description)!)
        //self.childNodeWithName("Player")?.position = CGPointMake(xPoint, (self.childNodeWithName("Player")?.position.y)! )
       let movimeinto = SKAction.moveTo(CGPointMake(xPoint,  (self.childNodeWithName("Player")?.position.y)!), duration: 0.0)
        self.childNodeWithName("Player")?.runAction(movimeinto)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var operation: CGFloat = 250.0
        operation *= (CGFloat) (self.value)
        
      self.childNodeWithName("Player")?.physicsBody?.applyForce(CGVectorMake(self.value, 20000))
        
    }
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody:SKPhysicsBody
        var secondBody:SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }else{
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
 //       print("A: " + contact.bodyA.categoryBitMask.description)
 //        print("B: " + contact.bodyB.categoryBitMask.description)
        if firstBody.categoryBitMask == 0 && secondBody.categoryBitMask == 1{
            puntos += 1
            self.childNodeWithName("Puntos")?.removeFromParent()
            score.text = "Puntuación: \(puntos)"
            self.addChild(score)
            secondBody.node?.removeFromParent()
            let delay = SKAction.waitForDuration(1)
            let generar = SKAction.runBlock({
                self.addChild(self.target)
            })
            let secuencia = SKAction.sequence([delay, generar])
            self.runAction(secuencia)
        }
    }
    

}
