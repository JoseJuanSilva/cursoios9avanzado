//
//  FadeTransition.swift
//  T2E04
//
//  Created by José Juan Silva Gamiño on 01/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class FadeTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval{
        return 2.0
    }

    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning){
        let toView: UIView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        let fromView: UIView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let containerView: UIView = transitionContext.containerView()!
        containerView.addSubview(toView)
        containerView.sendSubviewToBack(toView)
        let duration: NSTimeInterval = self.transitionDuration(transitionContext)
        UIView.animateWithDuration(duration, animations: {() in
           fromView.alpha = 0.0
            },         completion: {(finished) in
                if(transitionContext.transitionWasCancelled()){
                    fromView.alpha = 1.0
                } else{
                    fromView.removeFromSuperview()
                    fromView.alpha = 1.0
                }
                transitionContext.completeTransition(true)
        }) }

}
