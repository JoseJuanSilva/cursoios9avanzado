//
//  DestinoViewController.swift
//  T2E02
//
//  Created by José Juan Silva Gamiño on 01/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class DestinoViewController: UIViewController {

    @IBOutlet weak var lblPruebas: UILabel!
    var cadena : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let nuevoMensaje = cadena{
            lblPruebas.text = nuevoMensaje
        }
    }

    @IBAction func regresarAControladorPrincipal(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
  
}
