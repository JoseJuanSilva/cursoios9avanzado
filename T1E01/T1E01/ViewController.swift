//
//  ViewController.swift
//  T1E01
//
//  Created by José Juan Silva Gamiño on 29/03/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("JOSE JUAN SILVA GAMIÑO")
        
        var arrayOfNumbers = [Int]()
        for index in 1...10{
            arrayOfNumbers.append(index * 5)
        }
        let resultado = self.sumaArrayNumeros(arrayOfNumbers)
        print("El resultado es: \(resultado)")
    }

    func sumaArrayNumeros(numeros: [Int])->Int{
        var suma = 0
        for numero in numeros{
            suma = suma + numero
        }
        return suma
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

