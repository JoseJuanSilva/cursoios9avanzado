//
//  ViewController.swift
//  T2E04
//
//  Created by José Juan Silva Gamiño on 01/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIViewControllerTransitioningDelegate{
    var bounce : BounceTransition!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bounce = BounceTransition()
        
    }

   override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        if (segue.identifier == "showCustomTransition"){
            let toVC : UIViewController = segue.destinationViewController as UIViewController
            toVC.transitioningDelegate = self
        }
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.bounce
    }
}

