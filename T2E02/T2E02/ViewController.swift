//
//  ViewController.swift
//  T2E02
//
//  Created by José Juan Silva Gamiño on 31/03/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SeguePersonalizado"{
            if let destinoViewController = segue.destinationViewController as? DestinoViewController{
                destinoViewController.cadena  = "Hola mundo"
            }
        }
    }

}

