//
//  CalculadoraTest.swift
//  T1E02
//
//  Created by José Juan Silva Gamiño on 31/03/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import XCTest

@testable import T1E02
class CalculadoraTest: XCTestCase {
    var calculadora : Calculadora!
    override func setUp() {
        super.setUp()
        self.calculadora = Calculadora()
    }
    
    func testAdd(){
        self.calculadora.operator1 = 5
        self.calculadora.operator2 = 5
        self.calculadora.operation = "+"
        XCTAssertEqual(self.calculadora.makeOperation(), 10, "Error")
    }
    
    func testSub(){
        self.calculadora.operator1 = 10
        self.calculadora.operator2 = 5
         self.calculadora.operation = "-"
        XCTAssertEqual(self.calculadora.makeOperation(), 5, "Error")
        self.calculadora.operator2 = 20
        XCTAssertEqual(self.calculadora.makeOperation(), -10, "Error")
    }
    
    func testMul(){
        self.calculadora.operator1 = 10
        self.calculadora.operator2 = 5
        self.calculadora.operation = "*"
        XCTAssertEqual(self.calculadora.makeOperation(), 50, "Error")
        self.calculadora.operator2 = 20
        XCTAssertEqual(self.calculadora.makeOperation(), 200, "Error")
    }
    
    func testDiv(){
        self.calculadora.operator1 = 10
        self.calculadora.operator2 = 5
        self.calculadora.operation = "/"
        XCTAssertEqual(self.calculadora.makeOperation(), 2, "Error")
        self.calculadora.operator2 = 20
        XCTAssertEqual(self.calculadora.makeOperation(), 0.5, "Error")
    }
    
    func testDivByZero(){
        self.calculadora.operator1 = 10
        self.calculadora.operator2 = 0
        self.calculadora.operation = "/"
        XCTAssertEqual(self.calculadora.makeOperation(), 0, "Error")
       
    }
}
