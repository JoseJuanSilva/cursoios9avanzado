//
//  T1E02UITests.swift
//  T1E02UITests
//
//  Created by José Juan Silva Gamiño on 31/03/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import XCTest

class T1E02UITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        let t1e02Icon = app.scrollViews.otherElements.icons["T1E02"]
        t1e02Icon.tap()
        t1e02Icon.tap()
        t1e02Icon.tap()
        t1e02Icon.tap()
        
        let window = app.childrenMatchingType(.Window).elementBoundByIndex(0)
        window.tap()
        window.tap()
        window.tap()
        window.tap()
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
