//
//  ViewController.swift
//  T4E01
//
//  Created by José Juan Silva Gamiño on 15/04/16.
//  Copyright © 2016 José Juan Silva Gamiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var helpMe: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func buttonHelpMe(sender: AnyObject) {
        let contenidoPopover : UITableViewController = UITableViewController ()
        
    
        contenidoPopover.view.backgroundColor = UIColor.redColor()
        contenidoPopover.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        let etiqueta = UILabel(frame: CGRectMake(0, 150, 300, 50))
        etiqueta.text = "hola popover"
        etiqueta.textAlignment = NSTextAlignment.Center
        contenidoPopover.view.addSubview(etiqueta)
        
        let popover = contenidoPopover.popoverPresentationController as UIPopoverPresentationController?
        contenidoPopover.preferredContentSize = CGSizeMake(300,300)
        popover?.delegate = self
        popover?.sourceView = sender as! UIButton
        popover?.permittedArrowDirections = UIPopoverArrowDirection.Up
        popover?.sourceRect = CGRectMake(10, 20, 0, 0)
        self.presentViewController(contenidoPopover, animated: true, completion: nil)
        
 /*       let contenidoPopover : UIViewController = UIViewController ()
        contenidoPopover.view.backgroundColor = UIColor.redColor()
        contenidoPopover.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        let etiqueta = UILabel(frame: CGRectMake(0, 150, 300, 50))
        etiqueta.text = "hola popover"
        etiqueta.textAlignment = NSTextAlignment.Center
        contenidoPopover.view.addSubview(etiqueta)
        
        let popover = contenidoPopover.popoverPresentationController as UIPopoverPresentationController?
        contenidoPopover.preferredContentSize = CGSizeMake(300,300)
        popover?.delegate = self
        popover?.sourceView = sender as! UIButton
        popover?.permittedArrowDirections = UIPopoverArrowDirection.Up
        popover?.sourceRect = CGRectMake(10, 20, 0, 0)
        self.presentViewController(contenidoPopover, animated: true, completion: nil)*/
    }
}

